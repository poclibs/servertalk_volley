package com.servertalk.potingchiang.servertalk;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.servertalk.potingchiang.servertalk.objects.MyJson;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    //declaration
    //variables
    //tag
    private final static String TAG = "ServerTalk";
    //volley
    private RequestQueue mQueue;
    //volley get request
    private StringRequest getRequest;
    private StringRequest postRequest;
    //setup server uri/url
    //Ting's place
//    private final static String mUrl = "http://172.0.0.186:8886/POSTest.php";
    //Po's place
    private final static String mUrl = "http://192.168.0.52:8886/POSTest.php";
    //layout elements
    private TextView msg;
    private Button getMsg;
    //json/gson
    private Gson gson;
    private MyJson myJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init layout elements
        msg = (TextView) findViewById(R.id.msg);
        getMsg = (Button) findViewById(R.id.getMsg);
        //init. volley
        mQueue = Volley.newRequestQueue(MainActivity.this);
        //init gson
        gson = new Gson();
        //get message from server
        getRequest = new StringRequest(
                mUrl,
                new Response.Listener<String>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(String response) {

                        //regular String object
//                        msg.setText(response);
                        //json object
                        myJson = gson.fromJson(response, MyJson.class);
                        msg.setText("Id: " + myJson.getId() + "\n" +
                                "User Name: " + myJson.getUsername()
                        );

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //regular String object
                        msg.setText(error.getMessage());
                    }
                });
        //set on click listener
        getMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //trigger get request
                mQueue.add(getRequest);

                //messages for users
                Toast.makeText(MainActivity.this, "Getting server messages!", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Getting server messages!");
            }
        });

//        postRequest = new StringRequest(Request.Method.POST, mUrl,)

//        JsonObjectRequest
    }
}
