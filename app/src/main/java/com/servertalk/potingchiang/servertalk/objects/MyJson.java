package com.servertalk.potingchiang.servertalk.objects;

import com.google.gson.annotations.SerializedName;

/**
 * This is for json/gson practice
 * Created by potingchiang on 2016-06-12.
 */
public class MyJson {

    //declaration
    //gson inject
    @SerializedName("id")
    private String id;
    @SerializedName("username")
    private String username;

    //getter & setter
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
